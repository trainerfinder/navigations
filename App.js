import React from 'react';
import {StyleSheet,Text,View} from 'react-native';
import login from './components/login/login'
import profile from './components/profile/profile'
import details from './components/profile/details'
import { StackNavigator } from 'react-navigation'



const Application = StackNavigator({
    login: {screen: login} ,
    profile: {screen: profile} ,
    details: {screen: details}},
    {
        navigationOptions: {
        header: false
    }}
);

export default  class App extends React.Component {

  render() {
    return (
        <Application />
    );
  }
}

