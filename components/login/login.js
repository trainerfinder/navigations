import React, { Component } from 'react';
import {StyleSheet,Text,View, TextInput, AsyncStorage,KeyboardAvoidingView, TouchableOpacity} from 'react-native';


export default  class login extends React.Component {
    constructor(props){
      super(props);
      this.state = {
          username : '',
          password : '',
          failedlogin : false
      }
    }

    componentDidMount(){
        this._loadInitialState().done();
    }

    _loadInitialState = async () => {
         var value = await AsyncStorage.getItem('user');
         if(value !== null){
             this.props.navigation.navigate('profile');
         }
    }

    static navigationOptions = {
        title: 'Welcome to the safters',
    };

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>
                <View style={styles.container} >
                    <Text  style={styles.header}  >login</Text>
                    <TextInput style={styles.textInput}   placeholder='username' onChangeText={(username) => this.setState({username})}
                    underlineColorAndroid='transparent'
                    />
                    <TextInput style={styles.textInput}   placeholder='password' onChangeText={(password) => this.setState({password})}
                                underlineColorAndroid='transparent' secureTextEntry={true}
                    />
                    <TouchableOpacity style={styles.btn}   onPress={this.login}>
                        <Text>login</Text>
                    </TouchableOpacity>
                    {this.state.failedlogin ? <Text>you are fucked</Text> : null}
                </View>

            </KeyboardAvoidingView>

        );
    }

    login = () => {
        fetch('http://18.218.220.192:3000/logintest',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
            })
        .then((response) => response.json())
        .then((res) => {
           if(res.success === true){
                AsyncStorage.setItem('user', res.user);
                this.props.navigation.navigate('profile');
            }
            else{
               this.setState({failedlogin: true});
            }
        })
            .done()
    }


}

const styles = StyleSheet.create({
    wrapper:{
        flex :1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40
    },
    header: {
        fontSize: 24,
        marginBottom: 60,
        color: '#FFF',
        fontWeight: 'bold'
    },
    textInput: {
        alignSelf: 'stretch',
        padding:16,
        marginBottom:20,
        backgroundColor:'#fff'
    },
    btn:{
        alignSelf: 'stretch',
        backgroundColor: '#01c853',
        padding:20,
        alignItems: 'center'
    }
})

