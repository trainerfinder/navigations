/**
 * Created by zeev on 11/06/18.
 */
import React, { Component } from 'react';
import { StyleSheet,View, Image, Text, AsyncStorage,TouchableOpacity, FlatList} from 'react-native';
import { StackNavigator } from 'react-navigation'


class MyListItem extends React.PureComponent {
    _onPress = () => {
       // alert(this.props.data.category)
        this.props.onPressItem(this.props.data);
    };

    render() {
        const textColor = this.props.selected ? "red" : "black";
        return (
            <TouchableOpacity onPress={this._onPress}>
                <View>
                    <Text style={{ color: textColor }}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default class profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            filter1 : '',
            filter2 : '',
            dataSource : '',
            selected: (new Map(): Map<string, boolean>)
        }
    }

    static navigationOptions = {
        title: 'back to login',
    };

    componentDidMount(){
        this._loadInitialState().done();
    }

    _keyExtractor = (item, index) => item.id;

    _loadInitialState = async () => {
        this.getCategories();
    }


    _onPressItem = (item) => {

        // updater functions are preferred for transactional updates
        this.setState((state) => {
            // copy the map rather than modifying state.
           // const selected = new Map(state.selected);
           // selected.set(id, !selected.get(id)); // toggle
            this.props.navigation.navigate('details',{
                item: item   //your user details
            });
           // return {selected};
        });
    };

    _renderItem = ({item}) => (
        <MyListItem
            id={item._id}
            onPressItem = {(item)=>this._onPressItem(item)}
            data = {item}
            selected={!!this.state.selected.get(item._id)}
            title={item.category}
        />
    );

    render() {
        return (
            <View style={styles.container} >
                <Text  style={styles.header}  >welcome brother</Text>
                <FlatList
                    data={this.state.dataSource}
                    extraData={this.state}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        );
    }

    getCategories = () => {
        fetch('http://18.218.220.192:3000/workouts/search1',{
            method:'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((res) => {
                    this.setState({dataSource: res.workouts});
            })
            .done()
    }

    getCategories1 = () => {
        fetch('http://18.218.220.192:3000/workouts/search1',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filter1: this.state.filter1,
                filter2: this.state.filter2
            })
        })
            .then((response) => response.json())
            .then((res) => {
                this.setState({workouts: res});
            })
            .done()
    }
}


const styles = StyleSheet.create({
    wrapper:{
        flex :1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40
    },
    header: {
        fontSize: 24,
        marginBottom: 60,
        color: '#FFF',
        fontWeight: 'bold'
    },
    textInput: {
        alignSelf: 'stretch',
        padding:16,
        marginBottom:20,
        backgroundColor:'#fff'
    },
    btn:{
        alignSelf: 'stretch',
        backgroundColor: '#01c853',
        padding:20,
        alignItems: 'center'
    }
})


